﻿namespace tpHotel
{
    partial class frmVoirChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFermer = new System.Windows.Forms.Button();
            this.lstChambre = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.lstChambre)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(284, 234);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(97, 40);
            this.btnFermer.TabIndex = 4;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // lstChambre
            // 
            this.lstChambre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstChambre.Location = new System.Drawing.Point(35, 39);
            this.lstChambre.Name = "lstChambre";
            this.lstChambre.Size = new System.Drawing.Size(346, 175);
            this.lstChambre.TabIndex = 5;
            this.lstChambre.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstChambre_CellContentClick);
            // 
            // frmVoirChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 286);
            this.Controls.Add(this.lstChambre);
            this.Controls.Add(this.btnFermer);
            this.Name = "frmVoirChambre";
            this.Text = "frmVoirChambre";
            this.Load += new System.EventHandler(this.frmVoirChambre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstChambre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnFermer;
        private System.Windows.Forms.DataGridView lstChambre;
    }
}