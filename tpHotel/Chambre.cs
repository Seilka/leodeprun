﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        int num;
        int id;
        int etage;
        string description;
        

        public int Id { get => id; set => id = value; }
        
        public string Description { get => description; set => description = value; }
        public int Num { get => num; set => num = value; }
        public int Etage { get => etage; set => etage = value; }

        public Chambre(int UneId,int UnNum,int UnEtage, string UneDescription)
        {
            id = UneId;
            Etage = UnEtage;
            description = UneDescription;
            num = UnNum ;
        }
    }
    

}
