﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
          foreach(Hotel1 item in Persistance.getLesHotels())
            {
                if (item.Nom == cbHotel.Text)
                {
                    int id = item.Id;
                    Persistance.ajouteChambre((int)numEtage.Value, txtDescrption.Text,id);
                }
            }
        }
         
        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAnnuler_Click_1(object sender, EventArgs e)
        {
            numEtage.Value = 0;
            txtDescrption.Text = "";
            cbHotel.Text = "";
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            foreach(Hotel1 item in Persistance.getLesHotels())
            {
                cbHotel.Items.Add(item.Nom);
                 
            }
        }

        private void numEtage_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtDescrption_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
